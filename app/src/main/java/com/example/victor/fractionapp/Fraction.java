package com.example.victor.fractionapp;

/**
 * Created by victor on 27.10.16.
 */
public class Fraction {

    private int nominator;
    private int denominator;

    public Fraction () {
        nominator = 0;
        denominator = 1;
    }

    public Fraction (int n, int d) {
        nominator = n;
        denominator = d;
    }

    public Fraction (Fraction f) {
        nominator = f.nominator;
        denominator = f.denominator;
    }

    public Fraction add (Fraction f) {
        Fraction res = new Fraction();
        if(!(denominator==f.denominator)){
            nominator*=f.denominator;
            f.nominator*=denominator;
            denominator*= f.denominator;
            f.denominator*=denominator;
        }
        res.nominator=nominator+f.nominator;
        res.denominator=denominator;
        return res;
                //new Fraction(1,1);
    }


    public int getNominator() {
        return nominator;
    }

    public int getDenominator() {
        return denominator;
    }

    public Fraction sub(Fraction f2) {
        Fraction res = new Fraction();
        if(!(denominator==f2.denominator)){
            nominator*=f2.denominator;
            f2.nominator*=denominator;
            denominator*= f2.denominator;
            f2.denominator*=denominator;
        }
        res.nominator=nominator-f2.nominator;
        res.denominator=denominator;
        return res;
    }

    public Fraction mult(Fraction f2) {
        Fraction res = new Fraction();
        res.nominator=nominator*f2.nominator;
        res.denominator=denominator*f2.denominator;
        return res;
    }

    public Fraction div(Fraction f2) {

        Fraction res = new Fraction();
        res.nominator=nominator*f2.denominator;
        res.denominator=denominator*f2.nominator;
        return res;
    }
}
